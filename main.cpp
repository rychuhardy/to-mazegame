#include <iostream>
#include "GameObjects/Maze/Maze.h"
#include "MazeGame.h"
#include "MazeBuilder/SimpleMazeBuilder/SimpleMazeBuilder.h"

using namespace std;

int main() {

    SimpleMazeBuilder builder;

    std::unique_ptr<Maze> maze{ MazeGame::CreateSimpleMaze(builder)};

    Location location(MazeGame::GetStartingRoom(maze.get()));

    bool playing = true;

    while (playing) {
        loop:
        std::cout << "Use WSAD to move. Type q to quit\n";

        char input;
        cin >> input;
        Direction direction;
        switch (input) {
            case 'w':
                direction = Direction::North;
                break;
            case 's':
                direction = Direction::South;
                break;
            case 'a':
                direction = Direction::East;
                break;
            case 'd':
                direction = Direction::West;
                break;
            case 'q':
                playing = false;
                exit(EXIT_SUCCESS);
                //break;
                //TODO: Add cleanup
            default:
                std::cout << "Unrecognized option: " << input << std::endl;
                goto loop;
        }
        if(location.GetRoom()!=nullptr)
            location.GetRoom()->GetSide(direction)->Enter(location);

        if (MazeGame::IsFinalRoom(location.GetRoom().get())) {
            std::cout << "You have reached final room. Congratulations\n";
            std::cout << "Loading next level..." << std::endl;
            //TODO: Generate new maze
            location.SetRoom(MazeGame::GetStartingRoom(maze.get()));
        }


    }

    return 0;
}