//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_MAZE_H
#define MAZEGAME_MAZE_H


#include <map>
#include "../Room/Room.h"


class Maze {
public:
    Maze();

    void AddRoom(std::shared_ptr<Room> room);

    std::weak_ptr<Room> RoomNo(int);

private:
    std::map<int, std::shared_ptr<Room>> rooms;
};


#endif //MAZEGAME_MAZE_H
