//
// Created by ry on 5/15/16.
//

#include "Maze.h"

Maze::Maze() {

}

void Maze::AddRoom(std::shared_ptr<Room> room) {
    rooms[room->GetNumber()] = std::move(room);
}

std::weak_ptr<Room> Maze::RoomNo(int number) {
    std::weak_ptr<Room> ret = rooms[number];
    return ret;
}





