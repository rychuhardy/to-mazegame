//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_ROOM_H
#define MAZEGAME_ROOM_H


#include <array>
#include <memory>
#include <iostream>
#include "../../MazeBuilder/Location.h"
#include "../MapSite.h"


constexpr int SIDES_NUMBER = 4;

enum class Direction {
    North = 0,
    West = 1,
    East = 2,
    South = 3
};

class Room : public MapSite {
public:
    Room(int room_number);

    MapSite *GetSide(Direction) const;

    void SetSide(Direction, std::shared_ptr<MapSite>);

    void Enter(Location &location) override;

    int GetNumber() const;

private:
    int room_number;
    std::array<std::shared_ptr<MapSite>, SIDES_NUMBER> sides;
};


#endif //MAZEGAME_ROOM_H
