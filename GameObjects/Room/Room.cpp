//
// Created by ry on 5/15/16.
//

#include "Room.h"

Room::Room(int room_number) : room_number{room_number} { }

MapSite *Room::GetSide(Direction direction) const {
    return sides[static_cast<int>(direction)].get();
}

void Room::SetSide(Direction direction, std::shared_ptr<MapSite> side) {
    sides[static_cast<int>(direction)] = side;
}

void Room::Enter(Location &) {
    std::cout << "Entering room number: " << room_number << std::endl;
}

int Room::GetNumber() const {
    return room_number;
}









