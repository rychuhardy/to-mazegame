//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_WALL_H
#define MAZEGAME_WALL_H


#include "../MapSite.h"
#include "../../MazeBuilder/Location.h"
#include <iostream>

class Wall : public MapSite {
public:
    Wall();

    void Enter(Location &location) override;
};


#endif //MAZEGAME_WALL_H
