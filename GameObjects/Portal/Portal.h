//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_PORTAL_H
#define MAZEGAME_PORTAL_H


#include "../MapSite.h"
#include "../Room/Room.h"
#include "../../MazeBuilder/Location.h"
#include <iostream>


class Portal : public MapSite {
public:
    Portal(std::weak_ptr<Room> room, std::weak_ptr<Room> destination);

    virtual void Enter(Location &location) override;

private:
    std::weak_ptr<Room> room;
    std::weak_ptr<Room> destination;
};


#endif //MAZEGAME_PORTAL_H
