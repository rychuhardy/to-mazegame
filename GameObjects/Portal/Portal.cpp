//
// Created by ry on 5/15/16.
//

#include "Portal.h"


Portal::Portal(std::weak_ptr<Room> room, std::weak_ptr<Room> destination) : room{room}, destination{destination} {

}

void Portal::Enter(Location &location) {
    std::cout << "Entered a portal!" << std::endl;
    location.SetRoom(destination);
    location.GetRoom().get()->Enter(location);
}



