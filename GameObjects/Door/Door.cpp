//
// Created by ry on 5/15/16.
//

#include "Door.h"

Door::Door(std::weak_ptr<Room> room1, std::weak_ptr<Room> room2) : room1{room1}, room2{room2} {

}

void Door::Enter(Location &location) {
    std::cout << "Here we have doors..." << std::endl;
    auto current_room = location.GetRoom();
    auto new_room = OtherSideFrom(current_room);
    location.SetRoom(new_room);
    new_room.lock()->Enter(location);
}

std::weak_ptr<Room> Door::OtherSideFrom(std::weak_ptr<Room> room) {
    if (room.lock().get() == room1.lock().get())
        return room2;
    if (room.lock().get() == room2.lock().get())
        return room1;
    return std::weak_ptr<Room>{};
}





