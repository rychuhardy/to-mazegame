//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_DOOR_H
#define MAZEGAME_DOOR_H


#include <memory>
#include <iostream>
#include "../MapSite.h"
#include "../Room/Room.h"
#include "../../MazeBuilder/Location.h"


class Door : public MapSite {
public:
    Door(std::weak_ptr<Room>, std::weak_ptr<Room>);

    void Enter(Location &location) override;

    std::weak_ptr<Room> OtherSideFrom(std::weak_ptr<Room> room);


private:
    std::weak_ptr<Room> room1;
    std::weak_ptr<Room> room2;

};


#endif //MAZEGAME_DOOR_H
