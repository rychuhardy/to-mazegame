//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_MAPSITE_H
#define MAZEGAME_MAPSITE_H


//#include "../MazeBuilder/Location.h"

class Location;

class MapSite {
public:
    virtual void Enter(Location &location) = 0;

    virtual ~MapSite() = default;
};


#endif //MAZEGAME_MAPSITE_H
