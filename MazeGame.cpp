//
// Created by ry on 5/15/16.
//

#include "MazeGame.h"

Maze *MazeGame::CreateSimpleMaze(MazeBuilder &builder) {

    builder.BuildMaze();
    builder.BuildRoom(1);
    builder.BuildRoom(2);
    builder.BuildDoor(1, 2, Direction::North);

    builder.BuildRoom(3);
    builder.BuildDoor(2,3, Direction::East);

    builder.BuildRoom(4);
    builder.BuildDoor(3,4, Direction::North);
    builder.BuildRoom(5);

    builder.BuildDoor(4,5, Direction::West);

    return builder.GetMaze();

}

std::unique_ptr<Maze> MazeGame::CreateRandomMaze(MazeBuilder &) {
    //TODO: implement
    return nullptr;
}

std::weak_ptr<Room> MazeGame::GetStartingRoom(Maze *maze) {
    return maze->RoomNo(startingRoomNumber);
}

bool MazeGame::IsFinalRoom(Room *room) {

    return room->GetNumber() == finalRoomNumber;
}

int MazeGame::finalRoomNumber = 5;
int MazeGame::startingRoomNumber = 1;





