//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_LOCATION_H
#define MAZEGAME_LOCATION_H

#include <memory>
//#include "../GameObjects/Room/Room.h"

class Room;

class Location {

public:
    Location(std::weak_ptr<Room> room);

    void SetRoom(std::weak_ptr<Room> room);

    std::shared_ptr<Room> GetRoom();

private:
    std::weak_ptr<Room> room;
};


#endif //MAZEGAME_LOCATION_H
