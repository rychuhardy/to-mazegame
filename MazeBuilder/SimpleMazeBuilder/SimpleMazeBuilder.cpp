//
// Created by ry on 5/15/16.
//

#include "SimpleMazeBuilder.h"
#include "../../GameObjects/Wall/Wall.h"
#include "../../GameObjects/Door/Door.h"

SimpleMazeBuilder::SimpleMazeBuilder() : currentMaze{nullptr} { }

void SimpleMazeBuilder::BuildMaze() {
    currentMaze = new Maze();
}

void SimpleMazeBuilder::BuildRoom(int number) {
    if (currentMaze->RoomNo(number).expired()) {

        auto room = std::make_shared<Room>(number);

        room->SetSide(Direction::North, std::make_shared<Wall>());
        room->SetSide(Direction::East, std::make_shared<Wall>());
        room->SetSide(Direction::West, std::make_shared<Wall>());
        room->SetSide(Direction::South, std::make_shared<Wall>());

        currentMaze->AddRoom(room);
    }

}


void SimpleMazeBuilder::BuildDoor(int roomTo, int roomFrom, Direction direction) {
    auto r1 = currentMaze->RoomNo(roomTo);
    auto r2 = currentMaze->RoomNo(roomFrom);

    auto door = std::make_shared<Door>(r1, r2);

    r1.lock()->SetSide(direction, door);
    r2.lock()->SetSide(CommonWall(direction), door);


}

Maze *SimpleMazeBuilder::GetMaze() {
    return currentMaze;
}

Direction SimpleMazeBuilder::CommonWall(Direction direction) {
    //TODO: Reimplement;
    switch(direction) {
        case Direction::North : return Direction::South;
        case Direction::South :
            return Direction::North;
        case Direction::East :
            return Direction::West;
        case Direction::West :
            return Direction::East;
    }

    return Direction::North;
}

void SimpleMazeBuilder::BuildPortal(int) {
    //TODO: implement
}








