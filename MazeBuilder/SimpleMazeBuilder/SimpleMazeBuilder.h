//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_SIMPLEMAZEBUILDER_H
#define MAZEGAME_SIMPLEMAZEBUILDER_H


#include "../MazeBuilder.h"

class SimpleMazeBuilder : public MazeBuilder {

public:
    SimpleMazeBuilder();

    virtual void BuildMaze() override;

    virtual void BuildRoom(int number) override;

    virtual void BuildPortal(int roomFrom) override;

    virtual void BuildDoor(int roomTo, int roomFrom, Direction direction) override;

    virtual Maze *GetMaze() override;

private:
    Direction CommonWall(Direction direction);

    Maze *currentMaze;
};


#endif //MAZEGAME_SIMPLEMAZEBUILDER_H
