//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_MAZEBUILDER_H
#define MAZEGAME_MAZEBUILDER_H


#include <memory>
#include "../GameObjects/Maze/Maze.h"

class MazeBuilder {
public:
    virtual void BuildMaze();

    virtual void BuildRoom(int number);

    virtual void BuildPortal(int roomFrom);

    virtual void BuildDoor(int roomTo, int roomFrom, Direction direction);

    virtual Maze *GetMaze();

protected:
    MazeBuilder();
};


#endif //MAZEGAME_MAZEBUILDER_H
