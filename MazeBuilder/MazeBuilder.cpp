//
// Created by ry on 5/15/16.
//

#include "MazeBuilder.h"

void MazeBuilder::BuildMaze() { }

void MazeBuilder::BuildRoom(int) { }

void MazeBuilder::BuildPortal(int) { }

void MazeBuilder::BuildDoor(int, int, Direction) { }

Maze *MazeBuilder::GetMaze() { return nullptr; }

MazeBuilder::MazeBuilder() { }