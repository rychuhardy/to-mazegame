//
// Created by ry on 5/15/16.
//

#include "Location.h"


Location::Location(std::weak_ptr<Room> room) : room{room} {

}

void Location::SetRoom(std::weak_ptr<Room> room) {
    this->room = room;
}

std::shared_ptr<Room> Location::GetRoom() {
    return room.lock();
}





