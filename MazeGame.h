//
// Created by ry on 5/15/16.
//

#ifndef MAZEGAME_MAZEGAME_H
#define MAZEGAME_MAZEGAME_H


#include "GameObjects/Maze/Maze.h"
#include "MazeBuilder/MazeBuilder.h"

class MazeGame {
public:
    MazeGame() = delete;

    static Maze *CreateSimpleMaze(MazeBuilder &builder);

    static std::unique_ptr<Maze> CreateRandomMaze(MazeBuilder &builder);

    static std::weak_ptr<Room> GetStartingRoom(Maze *maze);

    static bool IsFinalRoom(Room *room);

private:
    static int finalRoomNumber;
    static int startingRoomNumber;
};


#endif //MAZEGAME_MAZEGAME_H
